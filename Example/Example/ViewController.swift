//
//  ViewController.swift
//  Example
//
//  Created by tonyli on 12/12/17.
//  Copyright © 2017 tonyli. All rights reserved.
//

import UIKit
import YYText
import YYHTMLAttributedStringBuilder
import SDWebImage
import GoogleMobileAds
import WebKit
import AXPhotoViewer

class ViewController: UIViewController, YYTextViewDelegate, GADBannerViewDelegate {
    
    // MARK - IBOutlet
    @IBOutlet weak var yyTextView: YYTextView!
    
    // MARK - private
    private var loaded:Bool = false
    private var fpsLabel: YYFPSLabel!
    private var attributedStringContent: NSMutableAttributedString!
    private var builder: YYHTMLAttributedStringBuilder!
    fileprivate var bounds: CGRect!
    fileprivate var photos = [Photo]()
    fileprivate var dataSource: PhotosDataSource!
    fileprivate var previewingPhotosViewController: PreviewingPhotosViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        yyTextView.delegate = self
        
        fpsLabel = YYFPSLabel.init(frame: CGRect(x: 20, y: self.view.bounds.height - 50, width: 55, height: 30))
        view.addSubview(fpsLabel)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        bounds = yyTextView.bounds
        
        if !loaded {
            do {
                let filename = "index"
                let extensionName = "html"
                guard let path = Bundle.main.url(forResource: filename, withExtension: extensionName) else {
                    return
                }
                let htmlData = try String(contentsOf: path, encoding: .utf8)
                let builderOptions: [String : Any] = [
                    "paragraphFont": UIFont.systemFont(ofSize: 18),
                    //                    "paragraphColor": UIColor.blue
                    ]
                builder = YYHTMLAttributedStringBuilder(forHtml: htmlData, options: builderOptions)
                builder.delegate = self
                attributedStringContent =  try builder.toNSMutableAttributedString()
                yyTextView.attributedText = attributedStringContent
                loaded = true
                
                dataSource = PhotosDataSource(photos: photos, initialPhotoIndex: 0, prefetchBehavior: .aggressive)
                previewingPhotosViewController = PreviewingPhotosViewController(dataSource: dataSource)
            } catch {
                print(error)
            }
        }else {
            attributedStringContent = builder.reloadContent(currentAttributedString: attributedStringContent, withUpdatedBounds: bounds)
            yyTextView.attributedText = attributedStringContent
            fpsLabel.frame = CGRect(x: 20, y: self.view.bounds.height - 50, width: 55, height: 30)
        }
    }
    
//    @IBAction func increaseFontSize(_ sender: Any) {
//        bounds = yyTextView.bounds
//        attributedStringContent = builder.increaseFontSize(currentAttributedString: attributedStringContent)
//        yyTextView.attributedText = attributedStringContent
//    }
//
//    @IBAction func decraseFontSize(_ sender: Any) {
//        bounds = yyTextView.bounds
//        attributedStringContent = builder.decreaseFontSize(currentAttributedString: attributedStringContent)
//        yyTextView.attributedText = attributedStringContent
//    }
}

extension ViewController: YYHTMLAttributedStringBuilderDelegate {
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        self.present(PhotosViewController(from: previewingPhotosViewController), animated: true)
    }
    
    func getBound() -> CGRect {
        return self.view.bounds
    }
    
    func longPressAction(linkURL: URL) {
        let actionSheetController = UIAlertController(title: nil, message: "\(linkURL)", preferredStyle: .actionSheet)
        // Create and add the Cancel action
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { [weak self] action -> Void in
            self?.dismiss(animated: true, completion: nil)
        }
        actionSheetController.addAction(cancelAction)
        // Create a copy action
        let copyAction = UIAlertAction(title: "Copy", style: .default) { [weak self] action -> Void in
            UIPasteboard.general.string = "\(linkURL)"
            self?.dismiss(animated: true, completion: nil)
        }
        actionSheetController.addAction(copyAction)
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    func setImage(imageURL: URL, completion: ((Any, CGSize) -> Void)) {
        let imageView = FLAnimatedImageView.init(frame: CGRect(x: 0, y: 0, width: bounds.width, height: (2/3)*bounds.width))
        imageView.contentMode = .scaleAspectFit
        imageView.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "download"), options: [], completed: nil)
        
        // add tap gusture for images
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(tapGestureRecognizer)
        
        photos.append(Photo.init(attributedTitle: nil, attributedDescription: nil, attributedCredit: nil, imageData: nil, image: nil, url: imageURL))
        
        completion(imageView, CGSize.init(width: bounds.width, height: (2/3)*bounds.width))
    }
    
    func setFigureImage(imageURL: URL, caption: String, completion: ((Any, CGSize) -> Void)) throws {
        var width: CGFloat = bounds.width
        if width > 300 {
            width = CGFloat(300.0)
        }
        let height = (2/3)*width
        let figcaptionView = FigureImageView(frame: CGRect(x: 0, y: 0, width: width, height: height+50), figcaptionString: caption)
        figcaptionView.figureImageView.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "download"), options: [], completed: nil)
        
        // add tap gusture for images
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        figcaptionView.figureImageView.isUserInteractionEnabled = true
        figcaptionView.figureImageView.addGestureRecognizer(tapGestureRecognizer)
        
        completion(figcaptionView, CGSize.init(width: width, height: height+50))
    }
    
    func setAdMob(completion: ((Any, CGSize) -> Void)) throws {
        var width: CGFloat = bounds.width
        if width > 300 {
            width = CGFloat(300.0)
        }
        let height: CGFloat = 300.0
        
        let adsView = GADBannerView.init(frame: CGRect(x: 0, y: 0, width: width, height: height))
        adsView.backgroundColor = UIColor.gray
        adsView.delegate = self
        adsView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        adsView.rootViewController = self
        
        DispatchQueue.main.async {
            adsView.load(GADRequest())
        }
        
        completion(adsView, CGSize.init(width: bounds.width, height: height))
    }
    
    func setIframe(html: String, completion: ((Any, CGSize) -> Void)) throws {
        var width: CGFloat = bounds.width
        if width > 300 {
            width = CGFloat(300.0)
        }
        let height = (2/3)*width
        let iframe = WKWebView.init(frame: CGRect(x: 0, y: 0, width: width, height: height))
        iframe.scrollView.isScrollEnabled = false
        
        DispatchQueue.main.async {
            iframe.loadHTMLString("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">"+html, baseURL: nil)
        }
        
        completion(iframe, CGSize.init(width: bounds.width, height: height))
    }
}


