Pod::Spec.new do |s|

  s.name = 'YYHTMLAttributedStringBuilder'
  s.version = '1.0.0'
  s.license = 'MIT'
  s.summary = 'A simple html to NSMutableAttributedString convertor for YYText.'
  s.description = <<-DESCRIPTION
                  contains html parser, NSattributedString builder, make life easier for display html to CoreText view with YYText
                  DESCRIPTION

  s.homepage = 'https://bitbucket.org/sphitd/yyhtmlattributedstringbuilder'
  s.author = { 'Tony Li' => 'tonyli@sph.com.sg' }
  s.social_media_url = 'https://bitbucket.org/sphtonyli/'

  s.source = {
    :git => 'https://sphtonyli@bitbucket.org/sphitd/yyhtmlattributedstringbuilder.git',
    :tag => '1.0.0'
  }

  s.ios.deployment_target  = '9.0'
  s.osx.deployment_target  = '9.0'

  s.source_files = 'YYHTMLAttributedStringBuilder/Sources/**/*'

  s.dependency 'SwiftSoup'
  s.dependency 'HTMLString', '~> 3.0'
  s.dependency 'YYText'

end
