//
//  YYHTMLAttributedStringBuilderTests.swift
//  YYHTMLAttributedStringBuilderTests
//
//  Created by tonyli on 12/12/17.
//  Copyright © 2017 tonyli. All rights reserved.
//

import XCTest
@testable import YYHTMLAttributedStringBuilder

class TestHTMLParserSpec: XCTestCase {
    
    override func setUp() {
        //
    }
    
    func testAllTags() {
        let filename = "allTags"
        let extensionName = "html"
        guard let path = Bundle(for: type(of: self)).url(forResource: filename, withExtension: extensionName) else {
            return
        }
        let html = try! String(contentsOf: path)
        let builder = YYHTMLAttributedStringBuilder.init(forHtml: html)
        builder.delegate = self
        do {
            let result = try builder.toNSMutableAttributedString()
            print(result)
        }catch {
            print(error.localizedDescription)
        }
    }
    
    func testImageNoSrcAttribute() {
        let filename = "linkSrcEmpty"
        let extensionName = "html"
        guard let path = Bundle(for: type(of: self)).url(forResource: filename, withExtension: extensionName) else {
            return
        }
        let html = try! String(contentsOf: path)
        let parser = YYHTMLParser.init(forHtml: html)
        parser.delegate = self
        do {
            try parser.parse()
        }catch {
            XCTAssertEqual(error as? YYHtmlParserError, YYHtmlParserError.getAttributeError)
        }
    }
    
    func testImageSrcNotValid() {
        let filename = "linkSrcNotValid"
        let extensionName = "html"
        guard let path = Bundle(for: type(of: self)).url(forResource: filename, withExtension: extensionName) else {
            return
        }
        let html = try! String(contentsOf: path)
        let parser = YYHTMLParser.init(forHtml: html)
        parser.delegate = self
        do {
            try parser.parse()
        }catch {
            XCTAssertEqual(error as? YYHtmlParserError, YYHtmlParserError.URLNotValid)
        }
    }
}

extension TestHTMLParserSpec: YYHTMLParserDelegate {
    
    func customizedVideoProvider() -> [String] {
        return ["https://www.facebook.com/video/embed", "https://www.youtube.com/embed/", "https://www.facebook.com/plugins/comment_embed", "http://www.straitstimes.com/embed/"]
    }
    
    func processImage(src: URL) {
        //
    }
    
    func processH2(string: String) {
        //
    }
    
    func processH3(string: String) {
        //
    }
    
    func processParagraph(text: String) {
        //
    }
    
    func processFigureImage(src: URL, caption: String) throws {
        //
    }
    
    func processStrong(string: String) {
        //
    }
    
    func processIframe(html: String) throws {
        //
    }
    
    func processLink(text: String, src: URL) {
        //
    }
    
    func processHr() throws {
        //
    }
    
    func processAdMob() throws {
        //
    }
    
    func processBr() throws {
        //
    }
}

extension TestHTMLParserSpec: YYHTMLAttributedStringBuilderDelegate {
    func longPressAction(linkURL: URL) {
        //
    }
    
    func getBound() -> CGRect {
        return CGRect(x: 0, y: 0, width: 370.0, height: 700.0)
    }
    
    func setImage(imageURL: URL, completion: ((Any, CGSize) -> Void)) {
        //
    }
    
    func setFigureImage(imageURL: URL, caption: String, completion: ((Any, CGSize) -> Void)) throws {
        //
    }
    
    func setAdMob(completion: ((Any, CGSize) -> Void)) throws {
        //
    }
    
    func setIframe(html: String, completion: ((Any, CGSize) -> Void)) throws {
        //
    }
}

