import UIKit

public class HrView: CAShapeLayer {
    
    public override init(layer: Any) {
        super.init()
    }
    
    public convenience init(size: CGRect) {
        self.init(layer: (Any).self)
        let linePath = UIBezierPath()
        linePath.move(to: CGPoint.init(x: -size.width, y: 0))
        linePath.addLine(to: CGPoint.init(x: size.width, y: 0))
        self.path = linePath.cgPath
        self.strokeColor = UIColor.gray.cgColor
        self.fillColor = UIColor.blue.cgColor
        self.isOpaque = true
        self.opacity = 1.0
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
