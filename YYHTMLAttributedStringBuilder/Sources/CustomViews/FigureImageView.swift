import UIKit

public class FigureImageView: UIView {
    public var figureImageView: UIImageView!
    private(set) var figcaption: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    public convenience init(frame: CGRect, figcaptionString: String) {
        self.init(frame: frame)
        figureImageView = UIImageView.init(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height - 50))
        self.addSubview(figureImageView)
        figcaption = UILabel.init(frame: CGRect(x: 0, y: self.frame.height - 50, width: self.frame.width, height: 50))
        figcaption.backgroundColor = UIColor.init(hexString: "#f1f1f1")
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.firstLineHeadIndent = 10.0
        paragraphStyle.headIndent = 10.0
        
        // - swift 3
         let figureAttributes: [String : Any] = [NSForegroundColorAttributeName: UIColor.gray, NSFontAttributeName: UIFont.systemFont(ofSize: 16), NSParagraphStyleAttributeName: paragraphStyle]
        
        let figureAttributedText = NSMutableAttributedString(string: figcaptionString, attributes: figureAttributes)
        figcaption.attributedText = figureAttributedText
        figcaption.numberOfLines = 0
        figcaption.lineBreakMode = .byWordWrapping
        figcaption.textAlignment = .natural
        self.addSubview(figcaption)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
