//
//  YYHtmlParserError.swift
//  Error Collections for YYHtmlParserError
//
//  Created by tonyli on 11/24/17.
//  Copyright © 2017 tonyli. All rights reserved.
//

import Foundation

public enum YYHtmlParserError: Error {
    case URLNotValid
    case getAttributeError
    case NoChildElements
    case NotSupported
}
