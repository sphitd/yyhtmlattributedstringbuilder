//
//  YYHtmlParser.swift
//  a simple html parser by using SwiftSoup & HTMLString
//
//  Created by tonyli on 12/8/17.
//

import SwiftSoup
import HTMLString

enum YYHtmlTag: String {
    case div
    case p
    case a
    case h2
    case h3
    case span
    case strong
    case iframe
    case img
    case figure
    case figcaption
    case br
    case hr
    case ads
}

protocol YYHTMLParserDelegate {
    func customizedVideoProvider() -> [String]
    func processImage(src: URL)
    func processH2(string: String)
    func processH3(string: String)
    func processParagraph(text: String)
    func processFigureImage(src: URL, caption: String) throws
    func processStrong(string: String)
    func processIframe(html: String) throws
    func processLink(text: String, src: URL)
    func processHr() throws
    func processAdMob() throws
    func processBr() throws
}

class YYHTMLParser {
    
    private var nodes: [Node]?
    private var html: String!
    
    // delegate
    var delegate: YYHTMLParserDelegate?
    
    init(forHtml html: String) {
        self.html = html
    }
    
    func parse() throws {
        if let nodes: [Node] = try SwiftSoup.parse(html).body()?.getChildNodes() {
            try iterateNodes(nodes)
        }else {
            throw YYHtmlParserError.NoChildElements
        }
    }
    
    private func iterateNodes(_ nodes: [Node]) throws {
        for node: Node in nodes {
            switch node.nodeName() {
            // change to check css selector than using html nameTag
            case YYHtmlTag.h3.rawValue:
                if node.childNodeSize() == 1 {
                    let text = try node.outerHtml().removeHtmlTag
                    delegate?.processH3(string: text.removingHTMLEntities)
                }else {
                    let nodeShadow = node.childNodesCopy()
                    for subNode in nodeShadow {
                        try iterateNodes([subNode])
                    }
                }
            case YYHtmlTag.h2.rawValue:
                if node.childNodeSize() == 1 {
                    let text = try node.outerHtml().removeHtmlTag
                    delegate?.processH2(string: text.removingHTMLEntities)
                }else {
                    let nodeShadow = node.childNodesCopy()
                    for subNode in nodeShadow {
                        try iterateNodes([subNode])
                    }
                }
            case YYHtmlTag.img.rawValue:
                if let urlString = node.getAttributes()?.get(key: "src"), urlString != "" {
                    if let url = URL(string: urlString) {
                        delegate?.processImage(src: url)
                    }else {
                        throw YYHtmlParserError.URLNotValid
                    }
                }else {
                    throw YYHtmlParserError.getAttributeError
                }
            case YYHtmlTag.figure.rawValue:
                let nodeShadow = node.childNodesCopy()
                var urlString: String = ""
                var caption: String = ""
                for subNode in nodeShadow {
                    if subNode.nodeName() == "img" {
                        if let imageUrlString = subNode.getAttributes()?.get(key: "src") {
                            urlString = imageUrlString
                        }else {
                            throw YYHtmlParserError.getAttributeError
                        }
                    }else if subNode.nodeName() == "figcaption" {
                        caption = try subNode.outerHtml()
                    }
                }
                
                if let url = URL(string: urlString) {
                    try delegate?.processFigureImage(src: url, caption: caption)
                }else {
                    throw YYHtmlParserError.URLNotValid
                }
            case YYHtmlTag.div.rawValue:
                if node.childNodeSize() == 0 {
                    let text = try node.outerHtml().removeHtmlTag
                    delegate?.processParagraph(text: text.removingHTMLEntities)
                }else {
                    let nodeShadow = node.childNodesCopy()
                    for subNode in nodeShadow {
                        if (subNode is TextNode) {
                            let text = try node.outerHtml().removeHtmlTag
                            delegate?.processParagraph(text: text.removingHTMLEntities)
                        }else {
                            try iterateNodes([subNode])
                        }
                    }
                }
            case YYHtmlTag.p.rawValue, YYHtmlTag.span.rawValue:
                if node.childNodeSize() == 0 {
                    let text = try node.outerHtml().removeHtmlTag
                    delegate?.processParagraph(text: text.removingHTMLEntities)
                }else {
                    let nodeShadow = node.childNodesCopy()
                    for subNode in nodeShadow {
                        if (subNode is TextNode) {
                            let text = try subNode.outerHtml().removeHtmlTag
                            delegate?.processParagraph(text: text.removingHTMLEntities)
                        }else {
                            try iterateNodes([subNode])
                        }
                    }
                }
            case YYHtmlTag.strong.rawValue:
                let text = try node.outerHtml().removeHtmlTag
                delegate?.processStrong(string: text.removingHTMLEntities)
            case YYHtmlTag.hr.rawValue:
                try delegate?.processHr()
            case YYHtmlTag.a.rawValue:
                let displayText = try node.outerHtml().removeHtmlTag
                if let hrefString = node.getAttributes()?.get(key: "href") {
                    if let url = URL(string: hrefString) {
                        delegate?.processLink(text: displayText.removingHTMLEntities, src: url)
                    }else {
                        throw YYHtmlParserError.URLNotValid
                    }
                }else {
                    throw YYHtmlParserError.getAttributeError
                }
            case YYHtmlTag.iframe.rawValue:
                if let urlString = node.getAttributes()?.get(key: "src") {
                    let videoProviders = (delegate?.customizedVideoProvider()) ?? []
                    for videoProvider in videoProviders {
                        if urlString.contains(videoProvider) {
                            try! node.removeAttr("height")
                            try! node.removeAttr("width")
                            try! node.attr("height", "100%")
                            try! node.attr("width", "100%")
                            let htmlString = String(describing: node)
                            try delegate?.processIframe(html: htmlString)
                        }
                    }
                }else {
                    throw YYHtmlParserError.getAttributeError
                }
            case YYHtmlTag.ads.rawValue:
                try delegate?.processAdMob()
            case YYHtmlTag.br.rawValue:
                try delegate?.processBr()
            default:
                /* for those text not inside html tag */
                if node is TextNode {
                    let text = try node.outerHtml().removeHtmlTag
                    delegate?.processParagraph(text: text.removingHTMLEntities)
                }else {
                    throw YYHtmlParserError.NotSupported
                }
                break
            }
        }
    }
}
