//
//  YYHTMLAttributedStringBuilderError.swift
//  Error Collections for YYStringBuilder
//
//  Created by tonyli on 12/8/17.
//

import Foundation

enum YYHTMLAttributedStringBuilderError: Error {
    case NoDelegateFound
}
