//
//  YYHTMLAttributedStringBuilder.swift
//  HTML -> YYHtmlParser -> (YYHTMLAttributedStringBuilder) -> NSMutableAttributedString
//
//  Created by tonyli on 12/8/17.
//

import YYText
import WebKit

public protocol YYHTMLAttributedStringBuilderDelegate {
    // TODO: if here get bound, check about the rest callback need CGSize or not
    func getBound() -> CGRect
    
    func setImage(imageURL: URL, completion:((Any, CGSize) -> Void))
    func setFigureImage(imageURL: URL, caption: String, completion:((Any, CGSize) -> Void)) throws
    func setAdMob(completion:((Any, CGSize) -> Void)) throws
    func setIframe(html: String, completion:((Any, CGSize) -> Void)) throws
    
    func longPressAction(linkURL: URL)
}

public class YYHTMLAttributedStringBuilder {
    // MARK: - public
    public var delegate: YYHTMLAttributedStringBuilderDelegate?
    
    // MARK: - private
    private var yyHtmlParser: YYHTMLParser?
    private var htmlString: String!
    fileprivate var videoProviders: [String] = ["https://www.facebook.com/video/embed", "https://www.youtube.com/embed/", "https://www.facebook.com/plugins/comment_embed", "http://players.brightcove.net"]
    fileprivate lazy var outputMutableAttributedString = NSMutableAttributedString()
    fileprivate var paragraphFont: UIFont = UIFont.systemFont(ofSize: 20)
    fileprivate var paragraphColor: UIColor = UIColor.init(hexString: "#333")
    fileprivate var firstLineHeadIndent: CGFloat = 0
    fileprivate var minimumLineHeight: CGFloat = 1.0
    fileprivate var paragraphSpacing: CGFloat = 5.0
    fileprivate lazy var rotationContents = [RotationContent]()
    fileprivate lazy var textContents = [TextContent]()
    
    public init(forHtml html: String) {
        htmlString = html.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    public convenience init(forHtml html: String, options: [String: Any]) {
        self.init(forHtml: html)
        loadCustomOptions(options)
    }
    
    public func toNSMutableAttributedString() throws -> NSMutableAttributedString {
        yyHtmlParser = YYHTMLParser(forHtml: htmlString)
        yyHtmlParser?.delegate = self
        do {
            try yyHtmlParser?.parse()
            outputMutableAttributedString.yy_minimumLineHeight = minimumLineHeight
            outputMutableAttributedString.yy_paragraphSpacing = paragraphSpacing
            return outputMutableAttributedString
        } catch {
            throw error
        }
    }
    
    public func increaseFontSize(currentAttributedString: NSMutableAttributedString) -> NSMutableAttributedString {
        paragraphFont = UIFont.systemFont(ofSize: paragraphFont.pointSize + 1)
        for textContent in textContents {
            let text = textContent.text
            let range = textContent.range
            let type = textContent.type
            if (type.rawValue == YYHtmlTag.p.rawValue) {
                let pAttributtes = NSMutableAttributedString()
                pAttributtes.yy_appendString(text)
                pAttributtes.yy_color = paragraphColor
                pAttributtes.yy_font = paragraphFont
                pAttributtes.yy_firstLineHeadIndent = firstLineHeadIndent
                pAttributtes.yy_lineBreakMode = NSLineBreakMode.byWordWrapping
                pAttributtes.yy_alignment = NSTextAlignment.left
                currentAttributedString.replaceCharacters(in: range, with: pAttributtes)
            }
        }
        return currentAttributedString
    }
    
    public func decreaseFontSize(currentAttributedString: NSMutableAttributedString) -> NSMutableAttributedString  {
        paragraphFont = UIFont.systemFont(ofSize: paragraphFont.pointSize - 1)
        for textContent in textContents {
            let text = textContent.text
            let range = textContent.range
            let type = textContent.type
            
            if (type.rawValue == YYHtmlTag.p.rawValue) {
                let pAttributtes = NSMutableAttributedString()
                pAttributtes.yy_appendString(text)
                pAttributtes.yy_color = paragraphColor
                pAttributtes.yy_font = paragraphFont
                pAttributtes.yy_firstLineHeadIndent = firstLineHeadIndent
                pAttributtes.yy_lineBreakMode = NSLineBreakMode.byWordWrapping
                pAttributtes.yy_alignment = NSTextAlignment.left
                currentAttributedString.replaceCharacters(in: range, with: pAttributtes)
            }
        }
        
        return currentAttributedString
    }
    
    public func reloadContent(currentAttributedString: NSMutableAttributedString, withUpdatedBounds bounds: CGRect) -> NSMutableAttributedString {
        for rotationContent in rotationContents {
            let content = rotationContent.content
            let range = rotationContent.range
            let type = rotationContent.type
            
            if (type.rawValue == YYHtmlTag.hr.rawValue) {
                let view = HrView.init(size: bounds)
                let attributedString = NSMutableAttributedString.yy_attachmentString(withContent: view, contentMode: UIViewContentMode.center, width: bounds.width, ascent: 2, descent: 20)
                currentAttributedString.replaceCharacters(in: range, with: attributedString)
            }
                
            if (type.rawValue == YYHtmlTag.img.rawValue) {
                if let imageContent = content as? UIImageView {
                    let attributedString = NSMutableAttributedString.yy_attachmentString(withContent: imageContent, contentMode: UIViewContentMode.center, width: bounds.width, ascent: imageContent.frame.height, descent: 0)
                    currentAttributedString.replaceCharacters(in: range, with: attributedString)
                }
            }
                
            if (type.rawValue == YYHtmlTag.ads.rawValue) {
                let attributedString = NSMutableAttributedString.yy_attachmentString(withContent: content, contentMode: UIViewContentMode.center, width: bounds.width, ascent: 300, descent: 0)
                currentAttributedString.replaceCharacters(in: range, with: attributedString)
            }
                
            if (type.rawValue == YYHtmlTag.iframe.rawValue) {
                if let wkWebContent = content as? WKWebView {
                    let attributedString = NSMutableAttributedString.yy_attachmentString(withContent: wkWebContent, contentMode: UIViewContentMode.center, width: bounds.width, ascent: wkWebContent.frame.height, descent: 0)
                    currentAttributedString.replaceCharacters(in: range, with: attributedString)
                }
            }
        }
        return currentAttributedString
    }
    
    private func loadCustomOptions(_ options: [String: Any]?) {
        if let options = options {
            if let this_videoProviders = options["videoProviders"] as? [String] {
                videoProviders = this_videoProviders
            }
            if let this_paragraphFont = options["paragraphFont"] as? UIFont {
                paragraphFont = this_paragraphFont
            }
            if let this_paragraphColor = options["paragraphColor"] as? UIColor {
                paragraphColor = this_paragraphColor
            }
            if let this_firstLineHeadIndent = options["firstLineHeadIndent"] as? CGFloat {
                firstLineHeadIndent = this_firstLineHeadIndent
            }
            if let this_minimumLineHeight = options["minimumLineHeight"] as? CGFloat {
                minimumLineHeight = this_minimumLineHeight
            }
        }
    }
}

extension YYHTMLAttributedStringBuilder: YYHTMLParserDelegate {
    
    func customizedVideoProvider() -> [String] {
        return videoProviders
    }
    
    func processImage(src: URL) {
        delegate?.setImage(imageURL: src, completion: { (content, bound) in
            let imageAttributedString = NSMutableAttributedString.yy_attachmentString(withContent: content, contentMode: .scaleAspectFit, width: bound.width, ascent: 10, descent: bound.height+10)
            let rotationContent = RotationContent(content: content, currentAttributedString: outputMutableAttributedString, size: bound, type: YYHtmlTag.img)
            rotationContents.append(rotationContent)
            outputMutableAttributedString.append(imageAttributedString)
        })
    }
    
    func processH2(string: String) {
        // TODO: find out the relations between website p and head font size
        // TODO: replace the drawView with CALayer
        let h2AttributedString = NSMutableAttributedString()
        let width = CGFloat(370.0)
        let drawView = UIView.init(frame: CGRect(x: 0, y: 0, width: 200, height: 1))
        drawView.backgroundColor = UIColor.init(hexString: "#D3D3D3")
        let drawAttributtes = NSMutableAttributedString.yy_attachmentString(withContent: drawView, contentMode: UIViewContentMode.center, width: width, ascent: 1, descent: 10.0)
        h2AttributedString.append(drawAttributtes)
        // text
        h2AttributedString.yy_appendString("\n\n\n"+string+"\n\n")
        h2AttributedString.yy_color = UIColor.init(hexString: "#D3D3D3")
        h2AttributedString.yy_font = UIFont.systemFont(ofSize: paragraphFont.pointSize*1.5)
        h2AttributedString.yy_lineBreakMode = NSLineBreakMode.byWordWrapping
        h2AttributedString.yy_alignment = NSTextAlignment.left
        h2AttributedString.yy_firstLineHeadIndent = firstLineHeadIndent
        h2AttributedString.yy_headIndent = 80
        h2AttributedString.yy_tailIndent = width - 80
        outputMutableAttributedString.append(h2AttributedString)
    }
    
    func processH3(string: String) {
        let h3AttributedString = NSMutableAttributedString()
        h3AttributedString.yy_appendString(string)
        h3AttributedString.yy_color = paragraphColor
        h3AttributedString.yy_font = UIFont.boldSystemFont(ofSize: paragraphFont.pointSize*1.17)
        h3AttributedString.yy_lineBreakMode = NSLineBreakMode.byWordWrapping
        h3AttributedString.yy_alignment = NSTextAlignment.left
        outputMutableAttributedString.append(h3AttributedString)
    }
    
    func processBr() throws {
        let brAttributedString = NSMutableAttributedString()
        brAttributedString.yy_appendString("\n")
        outputMutableAttributedString.append(brAttributedString)
    }
    
    func processParagraph(text: String) {
        let pAttributtes = NSMutableAttributedString()
        pAttributtes.yy_appendString(text)
        pAttributtes.yy_color = paragraphColor
        pAttributtes.yy_font = paragraphFont
        pAttributtes.yy_firstLineHeadIndent = firstLineHeadIndent
        pAttributtes.yy_lineBreakMode = NSLineBreakMode.byWordWrapping
        pAttributtes.yy_alignment = NSTextAlignment.left
        
        let textContent = TextContent.init(text: text,currentAttributedString: outputMutableAttributedString, type: YYHtmlTag.p, attributedStringSize: pAttributtes.length)
        
        textContents.append(textContent)
        
        outputMutableAttributedString.append(pAttributtes)
    }
    
    func processFigureImage(src: URL, caption: String) throws {
        if let delegate = self.delegate {
            do {
                try delegate.setFigureImage(imageURL: src, caption: caption, completion: { (content, bound) in
                    let figureImageViewAttachment = NSMutableAttributedString.yy_attachmentString(withContent: content, contentMode: UIViewContentMode.scaleToFill, width: bound.width, ascent: bound.height+50, descent: 25)
                    
                    let rotationContent = RotationContent(content: content, currentAttributedString: outputMutableAttributedString, size: bound, type: YYHtmlTag.figure)
                    rotationContents.append(rotationContent)
                    
                    outputMutableAttributedString.append(figureImageViewAttachment)
                })
            }catch {
                throw error
            }
        }else {
            throw YYHTMLAttributedStringBuilderError.NoDelegateFound
        }
    }
    
    func processStrong(string: String) {
        let sAttributtes = NSMutableAttributedString()
        sAttributtes.yy_appendString(string)
        sAttributtes.yy_color = paragraphColor
        sAttributtes.yy_font = UIFont.boldSystemFont(ofSize: paragraphFont.pointSize)
        sAttributtes.yy_lineBreakMode = NSLineBreakMode.byWordWrapping
        outputMutableAttributedString.append(sAttributtes)
    }
    
    func processIframe(html: String) throws {
        if let delegate = self.delegate {
            do {
                try delegate.setIframe(html: html, completion: { (content, bound) in
                    let iframeAttachment = NSMutableAttributedString.yy_attachmentString(withContent: content, contentMode: UIViewContentMode.center, width: bound.width, ascent: bound.height, descent: 0)
                    let rotationContent = RotationContent(content: content, currentAttributedString: outputMutableAttributedString, size: bound, type: YYHtmlTag.iframe)
                    rotationContents.append(rotationContent)
                    outputMutableAttributedString.append(iframeAttachment)
                })
            }catch {
                throw error
            }
        }else {
            throw YYHTMLAttributedStringBuilderError.NoDelegateFound
        }
    }
    
    func processLink(text: String, src: URL) {
        let linkAttributtes = NSMutableAttributedString()
        linkAttributtes.yy_appendString(text + " ")
        linkAttributtes.yy_color = UIColor.init(hexString: "#5A9DDC")
        let linkHightLight = YYTextHighlight()
        linkHightLight.tapAction = { (view, string, range, frame) in
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(NSURL(string:"\(src)")! as URL)
            } else {
                UIApplication.shared.openURL(NSURL(string:"\(src)")! as URL)
            }
        }
        linkHightLight.longPressAction = { [weak self] (view, string, range, frame) in
            self?.delegate?.longPressAction(linkURL: src)
        }
        linkHightLight.setBackgroundBorder(YYTextBorder(fill: UIColor.init(hexString: "#CCC"), cornerRadius: 4.0))
        linkAttributtes.yy_setTextHighlight(linkHightLight, range: NSRange.init(location: 0, length: linkAttributtes.length))
        linkAttributtes.yy_font = paragraphFont
        outputMutableAttributedString.append(linkAttributtes)
    }
    
    func processHr() throws {
        if let delegate = self.delegate {
            let bound = delegate.getBound()
            let hrView = HrView.init(size: bound)
            let attributedString = NSMutableAttributedString.yy_attachmentString(withContent: hrView, contentMode: UIViewContentMode.center, width: bound.width, ascent: 2, descent: 20)
            let rotationContent = RotationContent(content: hrView, currentAttributedString: outputMutableAttributedString, size: bound.size, type: YYHtmlTag.hr)
            rotationContents.append(rotationContent)
            outputMutableAttributedString.append(attributedString)
        }else {
            throw YYHTMLAttributedStringBuilderError.NoDelegateFound
        }
    }
    
    func processAdMob() throws {
        if let delegate = self.delegate {
            do {
                try delegate.setAdMob(completion: { (content, bound) in
                    let adsAttachment = NSMutableAttributedString.yy_attachmentString(withContent: content, contentMode: UIViewContentMode.center, width: bound.width, ascent: 300.0, descent: 50.0)
                    let rotationContent = RotationContent(content: content, currentAttributedString: outputMutableAttributedString, size: bound, type: YYHtmlTag.ads)
                    rotationContents.append(rotationContent)
                    outputMutableAttributedString.append(adsAttachment)
                })
            }catch {
                throw error
            }
        }else {
            throw YYHTMLAttributedStringBuilderError.NoDelegateFound
        }
    }
}

struct RotationContent {
    private(set) var content: Any
    private(set) var size: CGSize
    private(set) var type: YYHtmlTag
    private var currentAttributedString: NSMutableAttributedString
    private(set) var range: NSRange
    
    init(content: Any, currentAttributedString: NSMutableAttributedString, size: CGSize, type: YYHtmlTag) {
        self.content = content
        self.size = size
        self.type = type
        self.currentAttributedString = currentAttributedString
        self.range = NSRange.init(location: currentAttributedString.length, length: 1)
    }
}

struct TextContent {
    private(set) var text: String
    private(set) var type: YYHtmlTag
    private var attributedStringSize: Int
    private var currentAttributedString: NSMutableAttributedString
    private(set) var range: NSRange
    
    init(text: String, currentAttributedString: NSMutableAttributedString, type: YYHtmlTag, attributedStringSize: Int) {
        self.text = text
        self.type = type
        self.currentAttributedString = currentAttributedString
        self.attributedStringSize = attributedStringSize
        self.range = NSRange.init(location: currentAttributedString.length, length: attributedStringSize)
    }
}
