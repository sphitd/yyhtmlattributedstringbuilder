import Foundation
import UIKit

extension String {
    func contains(_ find: String) -> Bool{
        return self.range(of: find) != nil
    }
    
    func toDouble() -> Double? {
        return Double.init(self)
    }
    
    func toFloat() -> Float? {
        return Float.init(self)
    }
    
    var removeHtmlTag : String {
        var str: String = ""
        do {
            let regex =  "<[^>]+>"
            let expr = try NSRegularExpression(pattern: regex, options: NSRegularExpression.Options.caseInsensitive)
            str = expr.stringByReplacingMatches(in: self, options: [], range: NSMakeRange(0, self.count), withTemplate: "")
            //replacement is the result
        } catch {
            print(error)
        }
        
        return str
    }
}
